﻿using Domain.GitHost;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace DAL.Internal.Config;

public class YamlConfigRepository : IConfigRepository
{
    public String ConfigPath { get; private set; }
    private static MemoryConfigRepo _memory;

    public YamlConfigRepository()
    {
        ConfigPath = Environment.GetEnvironmentVariable("GITLINK_CONFIG") ??
                     Path.Combine(Directory.GetCurrentDirectory(), "config.yml");
        if (_memory == null)
        {
            Init();
        }
    }

    public IEnumerable<GitHost> GetHostsOfType(GitHostType type)
    {
        return _memory.GetHostsOfType(type);
    }

    public GitHost GetHost(string name)
    {
        return _memory.GetHost(name);
    }

    private void Init()
    {
        List<GitHost> hosts = new List<GitHost>();
        var deserializer = new DeserializerBuilder()
            .WithNamingConvention(CamelCaseNamingConvention.Instance)
            .Build();
        try
        {
            if (!File.Exists(ConfigPath))
            {
                GenerateDefault();
            }
        }
        catch (Exception e)
        {
            Console.Error.WriteLine("Error writing default config: "+e.StackTrace);
        }
        
        try
        {
            List<GitHost> loaded = deserializer.Deserialize<List<GitHost>>(File.ReadAllText(ConfigPath));
            loaded.ForEach(l =>
            {
                if (l.Auth.Equals("example"))
                {
                    Console.Error.WriteLine("Config contains an example host!");
                }
                else
                {
                    hosts.Add(l);
                }
            });
        }
        catch (Exception e)
        {
            Console.Error.WriteLine("Exception reading config file: "+e.StackTrace);
        }
        _memory = new MemoryConfigRepo(hosts);
    }
    
    private void GenerateDefault()
    {
        var defaultHost = new GitHost()
        {
            Name = "Gitlab",
            Type = GitHostType.Gitlab,
            Auth = "example",
            HostURL = "https://gitlab.com"
        };
        var serializer = new SerializerBuilder().WithNamingConvention(CamelCaseNamingConvention.Instance).Build();
        var serialized = serializer.Serialize(new List<GitHost> {defaultHost});
        File.WriteAllText(ConfigPath, serialized);
    }
}