﻿
using Domain.GitHost;

namespace DAL.Internal.Config;

public interface IConfigRepository
{
    public IEnumerable<GitHost> GetHostsOfType(GitHostType type);
    public GitHost GetHost(string name);
}