﻿using Domain.GitHost;

namespace DAL.Internal.Config;

public class MemoryConfigRepo : IConfigRepository
{
    private readonly List<GitHost> _hosts = new();

    public MemoryConfigRepo(params GitHost[] hosts)
    {
        _hosts.AddRange(hosts);
    }

    public MemoryConfigRepo(List<GitHost> hosts)
    {
        _hosts = hosts;
    }

    public IEnumerable<GitHost> GetHostsOfType(GitHostType type)
    {
        return _hosts.FindAll(h => h.Type == type);
    }

    public GitHost GetHost(string name)
    {
        return _hosts.FirstOrDefault(h => h.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))!;
    }
}