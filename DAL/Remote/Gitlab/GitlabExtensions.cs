﻿using Domain.ProjectManagement;
using GitLabApiClient.Models.Issues.Requests;
using GitLabApiClient.Models.Issues.Responses;
using Issue = Domain.ProjectManagement.Issue;

namespace DAL.Remote.Gitlab;

public static class GitlabExtensions
{
    public static Issue ToIssue(this GitLabApiClient.Models.Issues.Responses.Issue issue)
    {
        return new Issue
        {
            Author = issue.Author.Username,
            Closed = issue.State == IssueState.Closed,
            Title = issue.Title,
            Description = issue.Description,
            Id = issue.Iid.ToString(),
            Project = issue.ProjectId,
            Link = issue.WebUrl
        };
    }

    public static UpdatedIssueState GetGitlabState(this Issue issue)
    {
        if (issue.Closed)
            return UpdatedIssueState.Close;
        return UpdatedIssueState.Reopen;
    }

    public static CreateIssueRequest ToGitlab(this NewIssue issue)
    {
        return new CreateIssueRequest(issue.Title)
        {
            Description = issue.Description
        };
    }

    public static Project ToProject(this GitLabApiClient.Models.Projects.Responses.Project project)
    {
        return new Project
        {
            Id = project.Id.ToString(),
            Name = project.Name,
            Link = project.WebUrl,
            GroupId = project.Namespace.Id.ToString()
        };
    }
}