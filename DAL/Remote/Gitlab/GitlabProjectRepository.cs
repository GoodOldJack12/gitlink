﻿using Domain.ProjectManagement;
using GitLabApiClient;

namespace DAL.Remote.Gitlab;

public class GitlabProjectRepository : IProjectRepository
{
    private readonly IGitLabClient _client;

    public GitlabProjectRepository(string token, string endpoint = "https://gitlab.com")
    {
        _client = new GitLabClient(endpoint, token);
    }

    public bool IsProject(string id)
    {
        return GetProject(id) != null;
    }

    public bool IsProjectByName(string name)
    {
        return GetProjectByName(name) != null;
    }

    public Project? GetProject(string id)
    {
        return _client.Projects.GetAsync(id).Result?.ToProject();
    }

    public Project? GetProjectByName(string name)
    {
        var project = _client.Projects.GetAsync(p => p.Filter = name).Result
            .FirstOrDefault()?.ToProject();
        if (project != null && !project.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
            // project name is not an exact match
            return null;
        return project;
    }
}