﻿using Domain.ProjectManagement;
using GitLabApiClient;
using GitLabApiClient.Models.Issues.Requests;
using GitLabApiClient.Models.Issues.Responses;
using GitLabApiClient.Models.Notes.Requests;
using Issue = Domain.ProjectManagement.Issue;

namespace DAL.Remote.Gitlab;

public class GitlabIssueRepository : IssueRepository
{
    private readonly IGitLabClient _client;

    public GitlabIssueRepository(string token, string endpoint = "https://gitlab.com")
    {
        _client = new GitLabClient(endpoint, token);
    }

    /// <param name="issueId">Must be integer parsable</param>
    /// <exception cref="FormatException"> If <see cref="projectId" /> is not an integer</exception>
    public Issue? GetIssue(Project project, string issueId)
    {
        var issueInt = int.Parse(issueId);
        return _client.Issues.GetAllAsync(project.Id,project.GroupId,o=>
            {
                o.IssueIds = new List<int> { issueInt };
                o.State = IssueState.All;
            })
            .Result?.FirstOrDefault()?.ToIssue();
    }

    public IEnumerable<Issue> SearchIssues(Project project, string search)
    {
        return _client.Issues.GetAllAsync(project.Id, project.GroupId, options =>
            {
                options.Filter = search;
                options.State = IssueState.All;
            } ).Result
            .Select(i => i.ToIssue());
    }

    public Issue? UpdateIssue(Project project, Issue issue)
    {
        if (!IsIssue(project, issue.Id)) return null;
        return _client.Issues.UpdateAsync(project.Id, int.Parse(issue.Id), new UpdateIssueRequest
        {
            Description = issue.Description,
            State = issue.GetGitlabState()
        }).Result.ToIssue();
    }

    public bool IsIssue(Project project, string issueId)
    {
        return GetIssue(project, issueId) != null;
    }

    public Issue CreateIssue(Project project, NewIssue issue)
    {
        return _client.Issues.CreateAsync(project.Id, issue.ToGitlab()).Result.ToIssue();
    }

    public Issue? CloseIssue(Project project, string issueId, string comment = "")
    {
        var issue = GetIssue(project, issueId);
        if (issue == null) return null;

        if (comment.Any())
            _client.Issues.CreateNoteAsync(project.Id, int.Parse(issueId), new CreateIssueNoteRequest(comment));

        issue.Closed = true;
        return UpdateIssue(project, issue);
    }
}