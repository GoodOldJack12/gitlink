﻿using Domain.ProjectManagement;

namespace DAL.Remote;

public interface IProjectRepository
{
    bool IsProject(string id);
    bool IsProjectByName(string name);
    Project? GetProject(string id);
    Project? GetProjectByName(string name);
}