﻿using Domain.ProjectManagement;

namespace DAL.Remote;

public interface IssueRepository
{
    Issue? GetIssue(Project project, string issueId);
    IEnumerable<Issue> SearchIssues(Project project, string search);
    Issue? UpdateIssue(Project project, Issue issue);
    bool IsIssue(Project project, string issueId);
    Issue CreateIssue(Project project, NewIssue issue);
    Issue? CloseIssue(Project project, string issueId, string comment = "");
}