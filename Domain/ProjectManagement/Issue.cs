﻿namespace Domain.ProjectManagement;

public class Issue
{
    public string Id { get; set; }
    public string Project { get; set; }
    public string Author { get; set; }

    public string Title { get; set; }
    public string Description { get; set; }
    public bool Closed { get; set; }
    public string Link { get; set; }
}