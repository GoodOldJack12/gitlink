﻿namespace Domain.ProjectManagement;

public class Project
{
    public string Id { get; set; }
    public string GroupId { get; set; }
    public string Name { get; set; }
    public string Link { get; set; }
}