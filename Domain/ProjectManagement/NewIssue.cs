﻿namespace Domain.ProjectManagement;

public class NewIssue
{
    public string Title { get; set; }
    public string Description { get; set; }
}