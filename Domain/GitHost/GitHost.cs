﻿using System.ComponentModel.DataAnnotations;

namespace Domain.GitHost;

public class GitHost
{
    [Required]
    public string Name { get; set; }
    [Required]
    public GitHostType Type { get; set; }
    [Required]
    public String HostURL { get; set; }
    [Required]
    public String Auth { get; set; }
}