﻿using System;
using System.IO;
using DAL.Internal.Config;
using NUnit.Framework;

namespace Tests.DAL.Internal;

public class YamlConfigTest
{
    private YamlConfigRepository _config;
    string configPath =  Path.Combine(Directory.GetCurrentDirectory(), "config.yml");
    [SetUp]
    public void Setup()
    {
        File.Delete(configPath);
        Environment.SetEnvironmentVariable("GITLINK_CONFIG",configPath);
    }
    
    [Test]
    public void ShowDefault()
    {
        _config = new YamlConfigRepository();
        Console.WriteLine(File.ReadAllText(_config.ConfigPath));
    }
    
}