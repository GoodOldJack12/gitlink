using System;
using System.Linq;
using BL.ProjectManagement;
using BL.ProjectManagement.Issues;
using GitLabApiClient;
using GitLabApiClient.Models.Projects.Requests;
using NUnit.Framework;
using Project = GitLabApiClient.Models.Projects.Responses.Project;

namespace Tests.DAL.Remote;
[Explicit("Don't run remote tests automatically")]
public class Tests
{
    private static string token;
    private static readonly string host = "https://gitlab.com";
    private IIssueManager _manager;
    private Project _project;
    private IGitLabClient client;

    [OneTimeSetUp]
    public void Setup()
    {
        token = Environment.GetEnvironmentVariable("GITLINK_TEST_LAB_TOKEN") ?? throw new Exception("GITLINK_TEST_LAB_TOKEN not found!");
        _manager = IssueManagerFactory.Gitlab(token);
        client = new GitLabClient(host, token);
        try
        {
            var id = client.Projects.GetAsync(o => o.Filter = "TestProject").Result.First().Id;
            client.Projects.DeleteAsync(id);
        }
        catch (Exception e)
        {
            // ignored
        }

        _project = client.Projects.CreateAsync(CreateProjectRequest.FromName("TestProject")).Result;
    }

    [OneTimeTearDown]
    public void TearDown()
    {
        client.Projects.DeleteAsync(_project.Id);
    }
    [Test]
    public void CreateIssueWithReponame()
    {
        var title = _manager.CreateIssue(_project.Name, "Test - CreateWithRepoName").Title;
        Assert.True(client.Issues.GetAllAsync(_project.Id,_project.Namespace.Id,o=>o.Filter = title).Result.Any());
    }
    [Test]
    public void CreateAndRetrieveIssueByName()
    {
        var id = _manager.CreateIssue(_project.Name, "Test - RetrieveIssueByName").Id;
        Assert.DoesNotThrow(()=>_manager.GetIssue(_project.Name,id));
    }

    [Test]
    public void CreateAndRetrieveIssuesBySearch()
    {
        for (int i = 0; i < 3; i++)
        {
            _manager.CreateIssue(_project.Name, $"Test - Search{i}");
        }

        var issues = _manager.SearchIssues(_project.Name, "Test - Search");
        Assert.AreEqual(3,issues.ToList().Count);
    }

    [Test]
    public void CloseIssueBySearch()
    {
        var issuename = "Test - Close";
        _manager.CreateIssue(_project.Name, issuename);
        var result = _manager.SearchIssues(_project.Name, "Close").ToList();
        Assert.IsNotEmpty(result);
        var issue = result.FirstOrDefault(i => i.Title.Equals(issuename));
        Assert.IsNotNull(issue);
        _manager.CloseIssue(_project.Name, issue!.Id, "Closed by Test");
        Assert.True(_manager.GetIssue(_project.Name,issue.Id).Closed);
    }
}