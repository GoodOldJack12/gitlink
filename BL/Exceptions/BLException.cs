﻿namespace BL.Exceptions;

public class BLException : Exception
{
    public BLException(string message) : base(message)
    {
    }
}