﻿using Domain.GitHost;

namespace BL.HostManagement;

public interface IHostManager
{
    public GitHost GetHost(string name);
    public IEnumerable<GitHost> GetHosts();
    public IEnumerable<GitHost> GetHostsOfType(GitHostType type);

}