﻿using System.Collections;
using DAL.Internal.Config;
using Domain.GitHost;

namespace BL.HostManagement;

public class HostManager : IHostManager
{
    private IConfigRepository _repository;

    public HostManager(IConfigRepository repository)
    {
        _repository = repository;
    }

    public GitHost GetHost(string name)
    {
        return _repository.GetHost(name);
    }

    public IEnumerable<GitHost> GetHosts()
    {
        List<GitHost> hosts = new List<GitHost>();
        foreach (GitHostType type in Enum.GetValues(typeof(GitHostType)))
        {
            hosts.AddRange(GetHostsOfType(type));
        }

        return hosts;
    }

    public IEnumerable<GitHost> GetHostsOfType(GitHostType type)
    {
        return _repository.GetHostsOfType(type);
    }
}