﻿using BL.Exceptions;
using DAL.Remote;
using Domain.ProjectManagement;

namespace BL.ProjectManagement.Issues;

public class IssueManager : IIssueManager
{
    private readonly IssueRepository _issues;
    private readonly IProjectRepository _projects;

    public IssueManager(IssueRepository issues, IProjectRepository projects)
    {
        _issues = issues;
        _projects = projects;
    }

    public Issue GetIssue(string projectname, string id)
    {
        var project = ParseProject(projectname);
        VerifyIssue(project, id);
        return _issues.GetIssue(project, id)!;
    }

    public IEnumerable<Issue> SearchIssues(string projectname, string search)
    {
        var project = ParseProject(projectname);
        return _issues.SearchIssues(project, search);
    }

    public Issue CreateIssue(string projectname, string title, string description)
    {
        var project = ParseProject(projectname);
        return _issues.CreateIssue(project, new NewIssue { Title = title, Description = description });
    }

    public Issue CloseIssue(string projectname, string id, string message = "")
    {
        var project = ParseProject(projectname);
        VerifyIssue(project, id);
        return _issues.CloseIssue(project, id, message)!;
    }

    private void VerifyIssue(Project project, string issue)
    {
        try
        {
            if (!_issues.IsIssue(project, issue)) throw new BLException($"Issue #{issue} does not exist!");
        }
        catch (FormatException)
        {
            throw new BLException($"Invalid IssueId, must be an int!");
        }
    }

    public Project ParseProject(string project)
    {
        var result = _projects.GetProjectByName(project);
        if (result != null)
        {
            return result;
        }
        throw new BLException($"Project {project} does not exist!");
        // if (!(_projects.IsProject(project) || _projects.IsProjectByName(project)))
        // {
        //     throw new BLException($"Project {project} does not exist!");
        // }
        // if (_projects.IsProjectByName(project)) project = _projects.GetProjectByName(project)!.Id;
        //
        // return _projects.GetProject(project)!;
    }
}