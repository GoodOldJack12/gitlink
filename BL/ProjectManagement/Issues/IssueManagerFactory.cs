﻿using DAL.Remote.Gitlab;

namespace BL.ProjectManagement.Issues;

public static class IssueManagerFactory
{
    public static IIssueManager Gitlab(string token)
    {
        return new IssueManager(new GitlabIssueRepository(token), new GitlabProjectRepository(token));
    }
}