﻿using Domain.ProjectManagement;

namespace BL.ProjectManagement;

public interface IIssueManager
{
    Issue GetIssue(string projectname, string id);
    IEnumerable<Issue> SearchIssues(string projectname,string search);
    Issue CreateIssue(string project, string title, string description = "");
    Issue CloseIssue(string project, string id, string message = "");
}